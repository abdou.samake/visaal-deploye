import admin from "firebase-admin";
import {QuerySnapshot} from "@google-cloud/firestore";
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import DocumentReference = admin.firestore.DocumentReference;
import DocumentData = admin.firestore.DocumentData;
import DocumentSnapshot = admin.firestore.DocumentSnapshot;
const app = express();
app.use(cors());
app.use(bodyParser.json());
const db = admin.firestore();

const refAppareils = db.collection('appareils');

export async function getAllAppareils(): Promise<AppareilModel[]> {
    const appareilQuerySnap: QuerySnapshot = await refAppareils.get();
    const appareils: AppareilModel[] = [];
    appareilQuerySnap.forEach(appareilSnap => appareils.push(appareilSnap.data() as AppareilModel));
    return appareils;
}

export async function getAppareilById(appareilId: string): Promise<AppareilModel> {
    if (!appareilId) {
        throw new Error(`appareilId required`);
    }
    const appareilToFindRef = refAppareils.doc(appareilId);
    const appareilToFindSnap: DocumentSnapshot = await appareilToFindRef.get();
    if (appareilToFindSnap.exists) {
        return appareilToFindSnap.data() as AppareilModel;
    } else {
        throw new Error('object does not exists');
    }
}
async function testIfAppareilExistsById(appareilId: string): Promise<DocumentReference> {
    const appareilRef: DocumentReference = refAppareils.doc(appareilId);
    const snapAppareilToFind: DocumentSnapshot = await appareilRef.get();
    const appareilToFind: AppareilModel | undefined = snapAppareilToFind.data() as AppareilModel | undefined;
    if (!appareilToFind) {
        throw new Error(`${appareilId} does not exists`);
    }
    return appareilRef;
}
export async function postNewAppareil(newAppareil: AppareilModel): Promise<AppareilModel> {
    if (!newAppareil) {
        throw new Error(`new appareil must be filled`);
    }
    const addResult: DocumentReference<DocumentData> = await refAppareils.add(newAppareil);
    const createNewAppareil: DocumentReference = refAppareils
        .doc(addResult.id);
    await createNewAppareil.set({...newAppareil, id: createNewAppareil.id});

    return {...newAppareil, id: createNewAppareil.id};
}

export async function deleteAppareilById(appareilId: string): Promise<any> {
    if (!appareilId) {
        throw new Error('appareilId is missing');
    }
    const appareilToDeleteRef: DocumentReference = await testIfAppareilExistsById(appareilId);
    await appareilToDeleteRef.delete();
    return {response : `${appareilId} -> delete ok` };
}

export async function updateAppareil(appareilId: string, newAppareil: AppareilModel): Promise<AppareilModel> {
    if (!newAppareil || !appareilId) {
        throw new Error(`appareil data and patient id must be filled`);
    }

    const appareilToPatchRef: DocumentReference = await testIfAppareilExistsById(appareilId);
    await appareilToPatchRef.update(newAppareil);
    return newAppareil;
}
