import * as admin from "firebase-admin";
import QuerySnapshot = admin.firestore.QuerySnapshot;
import DocumentReference = admin.firestore.DocumentReference;
import DocumentData = admin.firestore.DocumentData;
import DocumentSnapshot = admin.firestore.DocumentSnapshot;
import {SoignantModel} from "./soignant.model";

const db = admin.firestore();

const refSoignants = db.collection('soignants');

export async function getAllSoignants(): Promise<SoignantModel[]> {
    const soignantQuerySnap: QuerySnapshot = await refSoignants.get();
    const soignants: SoignantModel[] = [];
    soignantQuerySnap.forEach(soignatSnap => soignants.push(soignatSnap.data() as SoignantModel));
    return soignants;
}
export async function getSoignantById(SoignantId: string): Promise<SoignantModel> {
    if (!SoignantId) {
        throw new Error(`SoignantId required`);
    }
    const soignantToFindRef = refSoignants.doc(SoignantId);
    const soignantToFindSnap: DocumentSnapshot = await soignantToFindRef.get();
    if (soignantToFindSnap.exists) {
        return soignantToFindSnap.data() as SoignantModel;
    } else {
        throw new Error('object does not exists');
    }
}
async function testIfSoignantExistsById(soignantId: string): Promise<DocumentReference> {
    const soignantRef: DocumentReference = refSoignants.doc(soignantId);
    const snapSoignantToFind: DocumentSnapshot = await soignantRef.get();
    const soignantToFind: SoignantModel | undefined = snapSoignantToFind.data() as SoignantModel | undefined;
    if (!soignantToFind) {
        throw new Error(`${soignantId} does not exists`);
    }
    return soignantRef;
}
export async function postNewSoignant(newSoignant: SoignantModel): Promise<SoignantModel> {
    if (!newSoignant) {
        throw new Error(`new product must be filled`);
    }
    const addResult: DocumentReference<DocumentData> = await refSoignants.add(newSoignant);
    const createNewSoignant: DocumentReference = refSoignants
        .doc(addResult.id);
    await createNewSoignant.set({...newSoignant, id: createNewSoignant.id});

    return {...newSoignant, id: createNewSoignant.id};
}

export async function deleteSoignantById(soignantId: string): Promise<any> {
    if (!soignantId) {
        throw new Error('patientId is missing');
    }
    const soignantToDeleteRef: DocumentReference = await testIfSoignantExistsById(soignantId);
    await soignantToDeleteRef.delete();
    return {response : `${soignantId} -> delete ok` };
}

export async function updateSoignant(soignantId: string, newSoignant: SoignantModel): Promise<SoignantModel> {
    if (!newSoignant || !soignantId) {
        throw new Error(`soignant data and patient id must be filled`);
    }

    const soignantToPatchRef: DocumentReference = await testIfSoignantExistsById(soignantId);
    await soignantToPatchRef.update(newSoignant);
    return newSoignant;
}
