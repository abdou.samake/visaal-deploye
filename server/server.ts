
import * as admin from "firebase-admin";
import {cert} from "./cred/cert";
admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://visaal-fc520.firebaseio.com"
});
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import {deletePatientById, getAllPatients, getPatientById, postNewPatient, updatePatient} from "./patients.services";
import {
    deleteSoignantById,
    getAllSoignants,
    getSoignantById,
    postNewSoignant,
    updateSoignant
} from "./soignant.service";
import {getAllAppareils, postNewAppareil} from "./appareil.service";
import {getAllUrines, getUrineById, postNewUrineData} from "./urines.service";


const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(cors());

app.get('/api/patients', async (req, res) => {
    try {
        const patients = await getAllPatients();
        return res.send(patients);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
app.get('/api/patients/:id', async (req, res) => {
    try {
        const patientId = req.params.id;
        const patientToFind = await getPatientById(patientId);
        return res.send(patientToFind);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
})

app.post('/api/patients', async (req, res) => {
    try {
        const newPatient = req.body;
        const addResult = await postNewPatient(newPatient);
        return res.send(addResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
app.patch('/api/patients/:id', async (req, res) => {
    try {
        const newPatient = req.body;
        const patientId = req.params.id
        const addResult = await updatePatient(patientId, newPatient);
        return res.send(addResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
app.delete('/api/patients/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const writeResult: string = await deletePatientById(id);
        return res.send(writeResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
app.get('/api/soignants', async (req, res) => {
    try {
        const soignants = await getAllSoignants();
        return res.send(soignants);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});

app.get('/api/soignants/:id', async (req, res) => {
    try {
        const soignantId: string = req.params.id;
        const soignantToFind = await getSoignantById(soignantId);
        return res.send(soignantToFind);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
})

app.post('/api/soignants', async (req, res) => {
    try {
        const newPatient = req.body;
        const addResult = await postNewSoignant(newPatient);
        return res.send(addResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
app.patch('/api/soignants/:id', async (req, res) => {
    try {
        const newPatient = req.body;
        const patientId = req.params.id
        const addResult = await updateSoignant(patientId, newPatient);
        return res.send(addResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
app.delete('/api/soignants/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const writeResult: string = await deleteSoignantById(id);
        return res.send(writeResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});

app.get('/api/appareils', async (req, res) => {
    try {
        const appareils = await getAllAppareils();
        return res.send(appareils);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
app.post('/api/appareils', async (req, res) => {
    try {
        const newAppareil = req.body;
        const addResult = await postNewAppareil(newAppareil);
        return res.send(addResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});

app.get('/api/urines', async (req, res) => {
    try {
        const urines = await getAllUrines();
        return res.send(urines);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});

app.get('/api/urines/:id', async (req, res) => {
    try {
        const urineId: string = req.params.id;
        const urine = await getUrineById(urineId);
        return res.send(urine);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
})
app.post('/api/patients/:id', async (req, res) => {
    try {
        const patientId = req.params.id;
        const newUrineData = req.body;
        const addResult = await postNewUrineData(newUrineData, patientId);
        return res.send(addResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});



app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});


