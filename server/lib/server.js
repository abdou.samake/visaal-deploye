"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const admin = __importStar(require("firebase-admin"));
const cert_1 = require("./cred/cert");
admin.initializeApp({
    credential: admin.credential.cert(cert_1.cert),
    databaseURL: "https://visaal-fc520.firebaseio.com"
});
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const patients_services_1 = require("./patients.services");
const soignant_service_1 = require("./soignant.service");
const appareil_service_1 = require("./appareil.service");
const urines_service_1 = require("./urines.service");
const app = express_1.default();
app.use(cors_1.default());
app.use(body_parser_1.default.json());
app.use(cors_1.default());
app.get('/api/patients', async (req, res) => {
    try {
        const patients = await patients_services_1.getAllPatients();
        return res.send(patients);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.get('/api/patients/:id', async (req, res) => {
    try {
        const patientId = req.params.id;
        const patientToFind = await patients_services_1.getPatientById(patientId);
        return res.send(patientToFind);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.post('/api/patients', async (req, res) => {
    try {
        const newPatient = req.body;
        const addResult = await patients_services_1.postNewPatient(newPatient);
        return res.send(addResult);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.patch('/api/patients/:id', async (req, res) => {
    try {
        const newPatient = req.body;
        const patientId = req.params.id;
        const addResult = await patients_services_1.updatePatient(patientId, newPatient);
        return res.send(addResult);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.delete('/api/patients/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const writeResult = await patients_services_1.deletePatientById(id);
        return res.send(writeResult);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.get('/api/soignants', async (req, res) => {
    try {
        const soignants = await soignant_service_1.getAllSoignants();
        return res.send(soignants);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.get('/api/soignants/:id', async (req, res) => {
    try {
        const soignantId = req.params.id;
        const soignantToFind = await soignant_service_1.getSoignantById(soignantId);
        return res.send(soignantToFind);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.post('/api/soignants', async (req, res) => {
    try {
        const newPatient = req.body;
        const addResult = await soignant_service_1.postNewSoignant(newPatient);
        return res.send(addResult);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.patch('/api/soignants/:id', async (req, res) => {
    try {
        const newPatient = req.body;
        const patientId = req.params.id;
        const addResult = await soignant_service_1.updateSoignant(patientId, newPatient);
        return res.send(addResult);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.delete('/api/soignants/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const writeResult = await soignant_service_1.deleteSoignantById(id);
        return res.send(writeResult);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.get('/api/appareils', async (req, res) => {
    try {
        const appareils = await appareil_service_1.getAllAppareils();
        return res.send(appareils);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.post('/api/appareils', async (req, res) => {
    try {
        const newAppareil = req.body;
        const addResult = await appareil_service_1.postNewAppareil(newAppareil);
        return res.send(addResult);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.get('/api/urines', async (req, res) => {
    try {
        const urines = await urines_service_1.getAllUrines();
        return res.send(urines);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.get('/api/urines/:id', async (req, res) => {
    try {
        const urineId = req.params.id;
        const urine = await urines_service_1.getUrineById(urineId);
        return res.send(urine);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.post('/api/patients/:id', async (req, res) => {
    try {
        const patientId = req.params.id;
        const newUrineData = req.body;
        const addResult = await urines_service_1.postNewUrineData(newUrineData, patientId);
        return res.send(addResult);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur :' + e.message });
    }
});
app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});
//# sourceMappingURL=server.js.map