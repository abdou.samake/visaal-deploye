"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const hostels_data_1 = require("../hostels.data");
function getHostels() {
    return hostels_data_1.hostelsData;
}
exports.getHostels = getHostels;
function addHostel(newHotel) {
    if (!newHotel) {
        throw new Error('new Hotel must be filled');
    }
    const hostels = getHostels();
    const id = hostels.length + 1;
    hostels.push(Object.assign({ id }, newHotel));
    return hostels;
}
exports.addHostel = addHostel;
function deleteHostel(hostelId) {
    const hostel = getHostels().filter((user) => user.id !== hostelId);
    const spread = getHostels().length - hostel.length;
    return (spread > 0 ? hostel : 'Cet hotel n\'existe pas');
}
exports.deleteHostel = deleteHostel;
function replaceHotel(hostelId, newHotel) {
    const id = hostelId;
    const hostels = getHostels();
    const index = hostels.findIndex((hostel) => hostel.id === hostelId);
    if (index == -1) {
        return "Désolé cet hotel n\'existe pas";
    }
    else {
        hostels[index] = Object.assign({ id }, newHotel);
        return hostels;
    }
}
exports.replaceHotel = replaceHotel;
function updateHostel(hostelId, toUpdate) {
    const id = hostelId;
    const hostels = getHostels();
    const index = hostels.findIndex((hostel) => hostel.id === hostelId);
    if (index == -1) {
        return "Désolé cet hotel n\'existe pas";
    }
    else {
        hostels[index] = Object.assign(Object.assign({}, hostels[index]), toUpdate);
        return hostels;
    }
}
exports.updateHostel = updateHostel;
//# sourceMappingURL=hotel.service.js.map