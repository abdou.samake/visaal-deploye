import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../services/auth.service';
import {Observable, of, Subject} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserModel} from '../models/user';
import {UserService} from '../services/user.service';
import {ModalsManagerService} from '../services/modals-manager.service';
import {catchError, filter, map, switchMap, takeUntil, tap} from 'rxjs/operators';
import {FirebaseError} from 'firebase';
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  destroy$: Subject<boolean> = new Subject<boolean>();

  index = 0;
  accountCreated = false;
  public createForm: FormGroup;
  public registerForm: FormGroup;

  user: UserModel;

  constructor(
    private fb: FormBuilder,
    private modalsManagerService: ModalsManagerService,
    private authService: AuthService,
    private userService: UserService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    const regexPhone = /^((\+)33|0)[1-9](\d{2}){4}$/;
    this.createForm = this.fb.group({
      civility: ['', Validators.required],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      birth_date: ['', Validators.required],
      zip_code: ['', Validators.required],
      phone: ['', [Validators.required, Validators.pattern(regexPhone)]],
      reseau_id: ['', Validators.required],
    });

    this.registerForm = this.fb.group({
      email: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      is_adult: [false, Validators.requiredTrue],
      is_cgu_validated: [false, Validators.requiredTrue],
    });

    this.userService.getUser$()
      .pipe(
        takeUntil(this.destroy$),
        filter(user => !!user.uid),
        tap((user) => this.user = user),
      )
      .subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  createAccountWithEmailAndPassword() {
    const {email, password} = this.registerForm.value;
    this.authService.createAccountWithEmailAndPassword$(email, password)
      .pipe(
        // switchMap((usercred) => this.userService.),
        map(() => this.modalsManagerService
          .info('Votre compte a bien été créé', `Votre compte  a bien été créé Vous pouvez passer à la page d'accueil.`),
        ),
        switchMap(ref => ref.afterClose.asObservable()),
        tap(() => this.accountCreated = true),
        tap(() => this.index = 1),
        tap(() => this.router.navigateByUrl('accueil')),
        catchError((err: FirebaseError) => {
          this.modalsManagerService.error(err.message);
          return of(err);
        }),
      )
      .subscribe();
  }

  onIndexChange(index: number): void {
    this.index = index;
  }

}
