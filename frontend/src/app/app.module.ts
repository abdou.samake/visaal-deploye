import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListPatientsComponent } from './patients/list-patients/list-patients.component';
import { AddPatientComponent } from './patients/add-patient/add-patient.component';
import { EditPatientComponent } from './patients/edit-patient/edit-patient.component';
import { AddSoignantComponent } from './soignants/add-soignant/add-soignant.component';
import { EditSoignantComponent } from './soignants/edit-soignant/edit-soignant.component';
import { LoginComponent } from './login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {environment} from '../environments/environment';
import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthService} from './services/auth.service';
import {HttpClientModule} from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import {Ng2SearchPipeModule} from 'ng2-search-filter';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import {NzGridModule} from 'ng-zorro-antd/grid';
import {NzFormModule} from 'ng-zorro-antd/form';
import { NotificationsComponent } from './notifications/notifications.component';
import {MatPseudoCheckboxModule} from '@angular/material/core';
import { ListSoignantComponent } from './soignants/list-soignant/list-soignant.component';
import { ListAppareilComponent } from './appareils/list-appareil/list-appareil.component';
import { AddAppareilComponent } from './appareils/add-appareil/add-appareil.component';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { DataPatientListComponent } from './patients/data-patient-list/data-patient-list.component';
import { DataPatientDetailsComponent } from './patients/data-patient-details/data-patient-details.component';
import { HomeDataPatientComponent } from './home-data-patient/home-data-patient.component';
import { DetailsPatientsComponent } from './patients/details-patients/details-patients.component';
import { HandleSoignantsComponent } from './soignants/handle-soignants/handle-soignants.component';
import { DetailsSoignantsComponent } from './soignants/details-soignants/details-soignants.component';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import {MessagingService} from './services/messaging.service';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatTableModule} from '@angular/material/table';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import {NzTableModule} from 'ng-zorro-antd/table';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatMenuModule} from '@angular/material/menu';
import {MatDialogModule} from '@angular/material/dialog';
import { ConfirmationDialogComponent } from './components/shared/confirmation-dialog/confirmation-dialog.component';
import {NzSpinModule} from 'ng-zorro-antd/spin';
import {NzModalModule} from 'ng-zorro-antd/modal';
import { FooterComponent } from './components/footer/footer.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import {ComponentsModule} from './components/components.module';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    ListPatientsComponent,
    AddPatientComponent,
    EditPatientComponent,
    AddSoignantComponent,
    EditSoignantComponent,
    LoginComponent,
    HomeComponent,
    SignUpComponent,
    NotificationsComponent,
    ListSoignantComponent,
    ListAppareilComponent,
    AddAppareilComponent,
    DataPatientListComponent,
    DataPatientDetailsComponent,
    HomeDataPatientComponent,
    DetailsPatientsComponent,
    HandleSoignantsComponent,
    DetailsSoignantsComponent,
    ConfirmationDialogComponent,
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        AngularFireModule.initializeApp(environment.firebase),
        MatIconModule,
        MatMenuModule,
        MatButtonModule,
        FormsModule,
        AngularFireAuthModule,
        HttpClientModule,
        ReactiveFormsModule,
        Ng2SearchPipeModule,
        AngularFirestoreModule,
        NzGridModule,
        NzFormModule,
      NzModalModule,
        NzTableModule,
        MatPseudoCheckboxModule,
        AngularFireDatabaseModule,
        AngularFireMessagingModule,
        MatToolbarModule,
        MatSidenavModule,
        MatTableModule,
        MatButtonToggleModule,
        MatMenuModule,
        MatDialogModule,
        NzSpinModule,
      ComponentsModule,
    ],
  entryComponents: [
    ConfirmationDialogComponent
  ],
  providers: [MessagingService, AuthService, { provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent]
})
export class AppModule { }
