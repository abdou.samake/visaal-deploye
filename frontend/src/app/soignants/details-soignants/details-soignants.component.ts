import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SoignantModel} from '../../models/soignant';
import {SoignantService} from '../../services/soignant.service';

@Component({
  selector: 'app-details-soignants',
  templateUrl: './details-soignants.component.html',
  styleUrls: ['./details-soignants.component.scss']
})
export class DetailsSoignantsComponent implements OnInit {


  @Input() soignant: SoignantModel;
  @Output() refreshList: EventEmitter<any> = new EventEmitter();
  currentSoignant: SoignantModel = null;
  message = '';

  constructor(private soignantService: SoignantService) { }
  ngOnInit(): void {
    this.message = '';
  }
  ngOnChanges(): void {
    this.message = '';
    this.currentSoignant = { ...this.soignant };
  }


  updateTutorial(): void {
    const data = {
      sex: this.currentSoignant.sex,
      lastName: this.currentSoignant.lastName,
      firstName: this.currentSoignant.firstName,
      dateOfBirthday: this.currentSoignant.dateOfBirthday,
      service: this.currentSoignant.service,
    };

    this.soignantService.updateSoignantById$(this.currentSoignant)
      .subscribe(() => this.message = 'mise à jour réussi!');
  }
}
