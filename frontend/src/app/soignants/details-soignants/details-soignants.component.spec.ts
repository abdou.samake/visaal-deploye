import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsSoignantsComponent } from './details-soignants.component';

describe('DetailsSoignantsComponent', () => {
  let component: DetailsSoignantsComponent;
  let fixture: ComponentFixture<DetailsSoignantsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsSoignantsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsSoignantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
