import { Component, OnInit } from '@angular/core';
import {SoignantModel} from '../../models/soignant';
import {SoignantService} from '../../services/soignant.service';
import {Router} from '@angular/router';
import {AngularFireAuth} from 'angularfire2/auth';
import {AuthService} from '../../services/auth.service';
import {PatientModel} from '../../models/patient';
import {ConfirmationDialogComponent} from '../../components/shared/confirmation-dialog/confirmation-dialog.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-list-soignant',
  templateUrl: './list-soignant.component.html',
  styleUrls: ['./list-soignant.component.scss']
})
export class ListSoignantComponent implements OnInit {
soignant: SoignantModel;
soignants: SoignantModel[];
  message = '';
  constructor(private soignantService: SoignantService,
              private router: Router,
              private afAuth: AngularFireAuth,
              public authService: AuthService,
              public dialog: MatDialog) { }
  filterTerm: string;
  ngOnInit(): void {
    this.getAllSoignants();
  }
  getAllSoignants(): void {
    this.soignantService.getAllSoignant$().subscribe(data => {
      this.soignants = data;
    });
  }
  addPatient(): void {
    this.router.navigate(['add-patient']);
  }
  disco(): void {
    this.afAuth.auth.signOut();
    // @ts-ignore
    this.router.navigateByUrl(['']);
  }
  updatePatient():
    void {
    this.router.navigateByUrl('edit-patient');
  }
  listePatient(): void {
    this.router.navigate(['list-patients']);
  }
  listSoignant(): void {
    this.router.navigate(['gerer-soignants']);
  }
  addSoignant(): void {
    this.router.navigate(['add-soignant']);
  }
  deletePatient(soignant: SoignantModel): void {
    this.soignantService.deleteSoignant$(soignant).catch(err => console.log(err));
      /*.subscribe(data => {
        console.log(data);
        this.getAllSoignants();
      });*/
  }
  accueil(){
    this.router.navigateByUrl('accueil');
  }
  deletSoignant(soignant: SoignantModel): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: 'Voulez vous vraiment supprimer le patient?'
    });
    dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log('Yes clicked');
          this.soignantService.deleteSoignant$(soignant).catch(err => console.log(err));
        }
      },
      () => this.message = 'suppression réussi!');
  }
}
