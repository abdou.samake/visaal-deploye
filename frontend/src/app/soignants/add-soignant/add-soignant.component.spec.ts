import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSoignantComponent } from './add-soignant.component';

describe('AddSoignantComponent', () => {
  let component: AddSoignantComponent;
  let fixture: ComponentFixture<AddSoignantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddSoignantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSoignantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
