import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HandleSoignantsComponent } from './handle-soignants.component';

describe('HandleSoignantsComponent', () => {
  let component: HandleSoignantsComponent;
  let fixture: ComponentFixture<HandleSoignantsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HandleSoignantsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HandleSoignantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
