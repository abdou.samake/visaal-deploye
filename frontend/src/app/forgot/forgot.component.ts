import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {ModalsManagerService} from '../services/modals-manager.service';
import {catchError, tap} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss']
})
export class ForgotComponent implements OnInit {
  email: string;

  constructor(
    private auth: AuthService,
    private modalsManagerService: ModalsManagerService,
  ) {
  }

  ngOnInit(): void {
  }

  onSubmit() {
    this.auth
      .resetPassword(this.email)
      .then(
        (x) => this.modalsManagerService.info('Demande reçue', 'Regardez vos mails.'),
        catchError(err => {
          this.modalsManagerService.error(err.message);
          return of(err);
        }),
      )

  }
}
