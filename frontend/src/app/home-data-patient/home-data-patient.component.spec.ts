import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeDataPatientComponent } from './home-data-patient.component';

describe('HomeDataPatientComponent', () => {
  let component: HomeDataPatientComponent;
  let fixture: ComponentFixture<HomeDataPatientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeDataPatientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeDataPatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
