import {Component, NgZone, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import {DomSanitizer} from '@angular/platform-browser';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ModalsManagerService} from "../services/modals-manager.service";
import {FirebaseError} from "firebase";
import {catchError, tap} from "rxjs/operators";
import {of, pipe} from "rxjs";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  loginForm: FormGroup;
  loading = false;

  constructor(
    private fb: FormBuilder,
    private modalsManagerService: ModalsManagerService,
    private authService: AuthService,
    private router: Router,
    private zone: NgZone,
  ) {
  }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      password: ['', Validators.required],
      email: ['', Validators.required],
    });
  }

  connectWithEmailAndPassword() {
    if (this.loginForm.invalid) {
      this.loginForm.markAllAsTouched();
      this.modalsManagerService.errorOnForm(this.loginForm);
      return;
    }
    const {email, password} = this.loginForm.value;
    this.loading = true;
    this.authService.connectWithEmailAndPassword$(email, password)
      .pipe(this.manageConnected$())
      .subscribe();
  }
  private manageConnected$() {
    return pipe(
      tap((x) => this.router.navigate(['/accueil'])),
      catchError((err: FirebaseError) => {
        this.modalsManagerService.error(err.message, () => this.loading = false);
        return of(err);
      }),
    );
  }
  signUp(){
    this.router.navigateByUrl('sign-up');
  }
  forgot(){
    this.router.navigateByUrl('forgot');
  }
}

