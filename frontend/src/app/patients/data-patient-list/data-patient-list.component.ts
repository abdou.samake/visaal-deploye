import { Component, OnInit } from '@angular/core';
import {map} from 'rxjs/operators';
import {DataPatientsService} from '../../services/data-patients.service';

@Component({
  selector: 'app-data-patient-list',
  templateUrl: './data-patient-list.component.html',
  styleUrls: ['./data-patient-list.component.scss']
})
export class DataPatientListComponent implements OnInit {


  tutorials: any;
  currentTutorial = null;
  currentIndex = -1;
  title = '';

  constructor(private dataPatientsService: DataPatientsService) { }

  ngOnInit(): void {
    this.retrieveTutorials();
  }

  refreshList(): void {
    this.currentTutorial = null;
    this.currentIndex = -1;
    this.retrieveTutorials();
  }

  retrieveTutorials(): void {
    this.dataPatientsService.getAll().snapshotChanges().pipe(
      map(changes =>
        changes.map(c =>
          ({ key: c.payload.key, ...c.payload.val() })
        )
      )
    ).subscribe(data => {
      this.tutorials = data;
    });
  }

  setActiveTutorial(tutorial, index): void {
    this.currentTutorial = tutorial;
    this.currentIndex = index;
  }

  removeAllTutorials(): void {
    this.dataPatientsService.deleteAll()
      .then(() => this.refreshList())
      .catch(err => console.log(err));
  }

}
