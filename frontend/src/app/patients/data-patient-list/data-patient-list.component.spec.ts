import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataPatientListComponent } from './data-patient-list.component';

describe('DataPatientListComponent', () => {
  let component: DataPatientListComponent;
  let fixture: ComponentFixture<DataPatientListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataPatientListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataPatientListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
