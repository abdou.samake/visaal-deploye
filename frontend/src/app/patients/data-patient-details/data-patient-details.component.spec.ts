import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataPatientDetailsComponent } from './data-patient-details.component';

describe('DataPatientDetailsComponent', () => {
  let component: DataPatientDetailsComponent;
  let fixture: ComponentFixture<DataPatientDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataPatientDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataPatientDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
