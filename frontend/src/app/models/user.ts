import {Timestamp} from 'rxjs';
import * as firebase from 'firebase';
import DocumentReference = firebase.firestore.DocumentReference;

export interface UserModel {
  uid: any;
  email?: string;
  userName?: string;
  firstName?: string;
  lastName?: string;
  gender?: string;

  zip?: string;
  phone?: string;
  birthday?: any;
  photoURL?: string;
  creationDate?: Timestamp<any>;
  displayName?: string;
  phoneNumber?: string;
  disabled?: boolean;
  notificationByMail?: boolean;

  emailVerified?: boolean;
  // validationState?: PatientValidationState;

  isUser?: boolean;
  isAdmin?: boolean;
  isManager?: boolean;
  isPds?: boolean;
}

export interface UserLink {
  userRef: DocumentReference;
  userUid: string;
  isUser?: boolean;
  isManager?: boolean;
}


export enum UserRolesEnum {
  isUser = 'isUser',
  isAdmin = 'isAdmin',
  isManager = 'isManager',
  isPds = 'isPds'
}
