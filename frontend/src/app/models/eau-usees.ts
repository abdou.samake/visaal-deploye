export interface EauUseeModele {
  date?: string;
  hour?: string;
  volume?: string;
  volume_Total?: string;
  id?: string;
}
