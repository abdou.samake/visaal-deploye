import { Injectable } from '@angular/core';
import {BehaviorSubject, from, Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {filter, tap} from 'rxjs/operators';
import {PatientModel} from '../models/patient';
import { AngularFirestore } from '@angular/fire/firestore';
import DocumentReference = firebase.firestore.DocumentReference;
import * as firebase from 'firebase';
import {UrinesModel} from '../models/urines';

@Injectable({
  providedIn: 'root'
})
export class PatientService {
  patient$: BehaviorSubject<PatientModel> = new BehaviorSubject<PatientModel>(null);
  patient: PatientModel;
  constructor(private http: HttpClient, private afs: AngularFirestore) { }
  getPatient$(): Observable<PatientModel> {
    return this.patient$
      .pipe(
        filter(patient => !!patient)
      );
  }

 getAllPatient$(): Observable<PatientModel[]> {
    return this.afs.collection('patients').valueChanges() as Observable<PatientModel[]>;
}

  getPatientById$(id: string): Observable<PatientModel>{
    return this.afs.collection('patients').doc(id).valueChanges() as Observable<PatientModel>;
  }

  // tslint:disable-next-line:typedef
  async addPatient$(patient: PatientModel) {
    const patientAdd: DocumentReference = await this.afs.collection('patients').add(patient);
    const createNewPatient = this.afs.collection('patients').doc(patientAdd.id);
    await createNewPatient.set({...patient, id: patientAdd.id});
    return patient;
  }


  deletePatient$(patient: PatientModel): Promise<any>{
    return (this.afs.collection('patients').doc(patient.id).delete());
  }

  deleteDataUrineById$(urine: UrinesModel): Promise<any>{
    return (this.afs.collection('urines').doc(urine.id).delete()
    );
  }
  /*updatePatient$(id: string, patient: {
    lastName: string;
    firstName: string;
    numberOfBed: number;
    dateOfBirthday: number;
    service: string;
    sex: string;
    numberOfRoom: number;
    numberOfDevice: number
  }): Observable<any>{
    return (this.http.patch(this.apiRest + 'patients' + '/' + id, patient) as Observable<any>);
  }*/

 updatePatientById$(patient: PatientModel): Observable<any> {
    return from(this.afs.collection('patients').doc(patient.id)
      .update({...patient})) as Observable<any>;
  }
  setPatientById$(patient: PatientModel): Observable<any> {
    return from(this.afs.collection('patients').doc(patient.id)
      .set({id: patient.id, lastName: patient.lastName,
        firstName: patient.firstName,
        dateOfBirthday: patient.dateOfBirthday,
        sex: patient.sex,
        service: patient.service,
        numberOfRoom: patient.numberOfRoom,
        numberOfDevice: patient.numberOfDevice,
        numberOfBed: patient.numberOfBed})) as Observable<any>;
  }

  /*getAllUrine(): Observable<any> {
    return this.http.get(this.apiRest + 'urines') as Observable<any>;
  }*/
  getUrineById(id: string): Observable<UrinesModel> {
    return this.afs.collection('urines').doc(id).valueChanges() as Observable<UrinesModel>;
  }
  }
