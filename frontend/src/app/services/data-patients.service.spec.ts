import { TestBed } from '@angular/core/testing';

import { DataPatientsService } from './data-patients.service';

describe('DataPatientsService', () => {
  let service: DataPatientsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataPatientsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
