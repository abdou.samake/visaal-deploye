import { Injectable } from '@angular/core';
import {BehaviorSubject, from, Observable} from 'rxjs';
import {UserModel} from '../models/user';
import {AngularFirestore} from '@angular/fire/firestore';
import {filter, tap} from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  user$: BehaviorSubject<UserModel> = new BehaviorSubject<UserModel>(null);

  constructor(
    private afs: AngularFirestore,
  ) {
  }

  getUser$(): Observable<UserModel> {
    return this.user$
      .pipe(
        filter(user => !!user)
      );
  }

  setUser(user: UserModel) {
    if (user) {
      this.user$.next(user);
    } else {
      this.user$.next(null);
    }
  }

  getMyData$(uid: string) {
    return this.getUserById$(uid)
      .pipe(
        tap((user: UserModel) => this.setUser(user))
      );
  }

  getUserById$(uid: string) {
    return this.afs.collection('users').doc(uid).valueChanges();
  }

  savePictureProfile$(uid: string, picture: string) {
    return from(this.afs.collection('users').doc(uid).set({photoURL: picture}, {merge: true}));
  }

  /* updateUserById$(uid: string, data: UserModel): Observable<void> {
     return from(this.afs.collection('users').doc<UserModel>(uid).update({...data, validationState: PatientValidationState.completed}));
   }*/
}
