import { Injectable } from '@angular/core';
import {DataPatient} from '../models/data-patient';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class DataPatientsService {
  private dbPath = '/Urine';

  tutorialsRef: AngularFireList<DataPatient> = null;

  constructor(private db: AngularFireDatabase) {
    this.tutorialsRef = db.list(this.dbPath);
  }

  getAll(): AngularFireList<DataPatient> {
    return this.tutorialsRef;
  }

  create(tutorial: DataPatient): any {
    return this.tutorialsRef.push(tutorial);
  }

  update(key: string, value: any): Promise<void> {
    return this.tutorialsRef.update(key, value);
  }

  delete(key: string): Promise<void> {
    return this.tutorialsRef.remove(key);
  }

  deleteAll(): Promise<void> {
    return this.tutorialsRef.remove();
  }
}
