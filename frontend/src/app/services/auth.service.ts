import {Injectable, NgZone} from '@angular/core';
import {auth, User} from 'firebase';
import {AngularFirestore} from '@angular/fire/firestore';
import {Router} from '@angular/router';
import {AngularFireAuth} from 'angularfire2/auth';
import {from, Observable} from 'rxjs';
import UserCredential = firebase.auth.UserCredential;
import {switchMap} from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authState: any = null;
  constructor(
    private afs: AngularFirestore,
    private afa: AngularFireAuth,
    private router: Router,
    private zone: NgZone,
    // private userService: UserService,
  ) {
    this.afa.authState.subscribe((user) => {
      this.authState = user;
    });
  }
  public seConnecter(userInfo: User){
    localStorage.setItem('ACCESS_TOKEN', 'access_token');
  }
  public estConnecte(){
    return localStorage.getItem('ACCESS_TOKEN') !== null;

  }
  public deconnecter(){
    localStorage.removeItem('ACCESS_TOKEN');
  }
  get isUserAnonymousLoggedIn(): boolean {
    return (this.authState !== null) ? this.authState.isAnonymous : false;
  }

  get currentUserId(): string {
    return (this.authState !== null) ? this.authState.name : '';
  }

  get currentUserName(): string {
    return this.authState.email;
  }

  get currentUser(): any {
    return (this.authState !== null) ? this.authState : null;
  }

  get isUserEmailLoggedIn(): boolean {
    if ((this.authState !== null) && (!this.isUserAnonymousLoggedIn)) {
      return true;
    } else {
      return false;
    }
  }
  connectWithEmailAndPassword$(email: string, password: string): Observable<UserCredential> {
    return (from(this.afa.auth.signInWithEmailAndPassword(email, password)) as Observable<UserCredential>);
  }

  createAccountWithEmailAndPassword$(email: string, password: string): Observable<UserCredential> {
    return from(this.afa.auth.createUserWithEmailAndPassword(email, password));
  }

  signOut$(): Observable<void> {
    return from(this.afa.auth.signOut());
  }
  resetEmail$(email: string): Observable<void> {
    return from(this.afa.auth.sendPasswordResetEmail(email));
  }
  resetPassword(email: string) {
    return this.afa.auth.sendPasswordResetEmail(email)
      .then(() => console.log('sent Password Reset Email!'))
      .catch((error) => console.log(error));
  }
  signOut(): void {
    this.afa.auth.signOut();
    console.log('coucou');
  }

}
