import { TestBed } from '@angular/core/testing';

import { ModalsManagerService } from './modals-manager.service';

describe('ModalsManagerService', () => {
  let service: ModalsManagerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ModalsManagerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
