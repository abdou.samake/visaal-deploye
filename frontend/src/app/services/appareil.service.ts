import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {PatientModel} from '../models/patient';
import {filter} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {AngularFirestore} from '@angular/fire/firestore';
import {AppareilModel} from '../models/appareil';
import DocumentReference = firebase.firestore.DocumentReference;
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class AppareilService {

  constructor(private http: HttpClient, private afs: AngularFirestore) { }
  getAllAppareils$(): Observable<AppareilModel[]>{
    return this.afs.collection('appareils').valueChanges() as Observable<AppareilModel[]>;
  }

  // tslint:disable-next-line:typedef
  async addAppareil$(appareil: AppareilModel){
    const appareilAdd: DocumentReference = await this.afs.collection('appareils').add(appareil);
    const createNewAppareil = this.afs.collection('appareils').doc(appareilAdd.id);
    await createNewAppareil.set({...appareil, id: appareilAdd.id});
    return appareil;
  }
}
