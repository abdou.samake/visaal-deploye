import { TestBed } from '@angular/core/testing';

import { ErrorFormService } from './error-form.service';

describe('ErrorFormService', () => {
  let service: ErrorFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ErrorFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
